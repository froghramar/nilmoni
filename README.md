# Nilmoni

### Installation
1. Clone the repository
2. Use **npm install** command

### Scripts
Use **npm run <*script_name*>**

| Script             | Use Case           |
|--------------------|:-------------------|
|build-server        | Build Server       |
|build-client        | Build Client       |
|run-server          | Run Server         |
|build-and-run-server| Build + Run Server |
