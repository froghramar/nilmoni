import {
	getAbsoluteDirection, getNextDirection, getNextPoint,
	getRelativeDirection
} from "../shared/loggic/movement";
import {Point} from "../shared/geometry/point";
import {Constants} from "../shared/types/Constants";

export class Canvas {

    private context: CanvasRenderingContext2D;
    private _background: string;
    public currentDirection: number;
    public currentPosition: Point;
    public targetPosition: Point;

    constructor() {}

	init(){
		this.currentPosition =  new Point(this.width / 2, this.height / 2);
		this.targetPosition = new Point(this.width / 2, this.height / 2);
		this.currentDirection = 0;
		this.context.lineWidth = Constants.SNAKE_WIDTH;
	}

    set canvasContext(canvasContext: CanvasRenderingContext2D) {
        this.context = this.context || canvasContext;
    }

    set width(width: number) {
		this.context.canvas.width = width;
	}

	set height(height: number) {
		this.context.canvas.height = height;
	}

    get width(): number {
		return this.context.canvas.width;
	}

	get height(): number {
		return this.context.canvas.height;
	}

    draw() {
    	this.context.clearRect(0, 0, this.width, this.height);
		this.context.fillStyle = this._background;
		this.context.fillRect(0, 0, this.width, this.height);
		this.context.strokeStyle = 'pink';
    }

    add_point() {
    	const targetDir = getNextDirection(this.currentPosition, this.currentDirection, this.targetPosition);
		const relativeDir = getRelativeDirection(this.currentDirection, targetDir);
		let absoluteDir = getAbsoluteDirection(this.currentDirection, relativeDir);

		const point = getNextPoint(this.currentPosition, absoluteDir);
		this.context.moveTo(this.currentPosition.x, this.currentPosition.y);
		this.context.lineTo(point.x, point.y);
		this.context.stroke();
		this.currentPosition = point;

		this.currentDirection = absoluteDir;
	}

    set background(color: string) {
    	this._background = color;
	}

    getRoundedRect(x: number, y: number, width: number, height: number, radius: number): Path2D {

        const path = new Path2D();
        path.moveTo(x, y + radius);
        path.lineTo(x, y + height - radius);
        path.arcTo(x, y + height, x + radius, y + height, radius);
        path.lineTo(x + width - radius, y + height);
        path.arcTo(x + width, y + height, x + width, y + height-radius, radius);
        path.lineTo(x + width, y + radius);
        path.arcTo(x + width, y, x + width - radius, y, radius);
        path.lineTo(x + radius, y);
        path.arcTo(x, y, x, y + radius, radius);

        return path;
    }

}
