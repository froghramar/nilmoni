import { Canvas } from './Canvas';
import { Point } from "../shared/geometry/point";
import Timer = NodeJS.Timer;

const win = window as Window;
const canvas = new Canvas();
let redrawInterval: Timer;

win.onload = () => {
    const canvasElement = document.getElementById('game-field') as HTMLCanvasElement;
    const canvasContext = canvasElement.getContext('2d') as CanvasRenderingContext2D;

    canvasElement.onclick = (event: MouseEvent) => {
    	const point = new Point(event.clientX, event.clientY);
		canvas.targetPosition = point;
	};

    redrawInterval = setInterval(() => {
    	canvas.add_point();
	}, 100);

    canvas.canvasContext = canvasContext;
    canvas.background = 'grey';
    canvas.height = win.innerHeight;
	canvas.width = win.innerWidth;
	canvas.init();
    canvas.draw();
};

win.onresize = () => {
	canvas.height = win.innerHeight;
	canvas.width = win.innerWidth;
	canvas.init();
	canvas.draw();
};

win.onkeydown = (event: KeyboardEvent) => {
    const key = event.key;
    switch(key) {
        case 'Delete':
            clearInterval(redrawInterval);
            break;
    }
};
