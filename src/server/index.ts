import { config } from 'dotenv';
import { WebSocketServer } from './websocket-server';

config();

const webSocketServer = new WebSocketServer(+process.env.SOCKET_PORT);
webSocketServer.listen();
