import * as express from 'express';
const router = express.Router();

router.get('/', (request, response, callback) => {
	response.status(200).send("Ok");
});

export const Router = router;
