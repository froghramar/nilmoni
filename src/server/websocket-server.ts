import {
    server as WsServer,
    IMessage,
    request as WebSocketRequest
} from 'websocket';
import {
    createServer as createHttpServer,
    Server as HttpServer
} from 'http';

export class WebSocketServer {

    private wsServer: WsServer;
    private httpServer: HttpServer;

    constructor(private port: number) {
        this.init();
    }

    private init() {
        this.initializeHttpServer();
        this.initializeWsServer();
    }

    private initializeHttpServer() {
        this.httpServer = createHttpServer((request, response) => {
            response.writeHead(404);
            response.end();
        });

        this.httpServer.listen(this.port, () => {
            console.log((new Date()) + ' Server is listening on port ' + this.port);
        });
    }

    private initializeWsServer() {
        this.wsServer = new WsServer({
            httpServer: this.httpServer,
            autoAcceptConnections: false
        });
    }

    listen() {
        this.wsServer.on('request', (request: WebSocketRequest) => {
            if (!this.originIsAllowed(request.origin)) {
                request.reject();
                return;
            }
            const connection = request.accept('echo-protocol', request.origin);
            connection.on('message', (message: IMessage) => {
                if (message.type === 'utf8') {
                    console.log('Received Message: ' + message.utf8Data);
                    connection.sendUTF(message.utf8Data);
                }
            });
            connection.on('close', () => {
                console.log('Connection closed from ' + request.origin);
            });
        });
    }

    originIsAllowed(origin: string) {
        return true;
    }

}
