export interface IPoint {
    x: number,
    y: number,
    theta?: number
}


export class Point implements IPoint {
    constructor(public x: number, public y: number, public theta?: number) {}
    getDistance(anotherPoint: IPoint):number{
        let distance:number;
        distance =  Math.sqrt(Math.pow(this.x - anotherPoint.x, 2) + Math.pow(this.y - anotherPoint.y, 2));
        return distance;
    }
}
