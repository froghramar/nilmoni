/**
 * y = mx + c
 **/

import { IPoint } from './point';

export interface IStraightLine {
    x: number,
    y: number,
    slope: number,
    intercept: number
}

export class StraightLine implements IStraightLine {
    x: number;
    y: number;
    slope: number;
    intercept: number;
    constructor(xOrPoint?: number | IPoint, yOrPoint?: number | IPoint,  _slope = 1,  _intercept = 0) {
        if(!xOrPoint && !yOrPoint){
            throw TypeError("X and Y both can't be empty");
        } else if(xOrPoint && typeof(xOrPoint)=="object" && yOrPoint && typeof(yOrPoint) == "object"){
            this.initFromTwoPoints(xOrPoint, yOrPoint);
        } else if((xOrPoint && typeof(xOrPoint) == 'number') || (yOrPoint && typeof(yOrPoint) == 'number')){
            if(typeof(xOrPoint) == 'number')
                this.x = xOrPoint;
            if(typeof(yOrPoint) == 'number')
                this.y = yOrPoint;

            this.slope = _slope;
            this.intercept = _intercept;
        }
        
    }

    private initFromTwoPoints(p1:IPoint, p2:IPoint) {
        let newLine: StraightLine;
        this.x = 10;
    }

   
    getIntersectingLine(intersectingPoint: IPoint, theta: number): IStraightLine {
        let line: IStraightLine;


        return line;
    }

}