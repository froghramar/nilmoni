import { Constants } from "../types/Constants";
import { Point } from "../geometry/point";

export function getNextDirection(currentPosition: Point, currentDirection: number,
								 targetPosition: Point): number {
	const diff = new Point(targetPosition.x - currentPosition.x,
		targetPosition.y - currentPosition.y);

	let targetDirection: number;

	if(diff.x == 0) {
		targetDirection = (diff.y < 0) ? Constants.HALF_PI : 1.5 * Constants.PI;
	}
	else if(diff.y >= 0){
		let angle = Math.atan2(diff.y, Math.abs(diff.x));
		if(diff.x < 0) {
			angle = Math.PI - angle;
		}
		targetDirection = angle
	}
	else {
		let angle = Math.atan2(Math.abs(diff.y), Math.abs(diff.x));
		if(diff.x < 0) {
			angle = Math.PI - angle;
		}
		targetDirection = Constants.TWO_PI - angle;
	}

	return targetDirection;
}

export function getRelativeDirection(currentDirection: number,
									 targetDirection: number): number {

	const theta1: number = currentDirection;
	const theta2: number = targetDirection;

	const clockwiseDistance = theta2 > theta1 ? theta2 - theta1 : theta2 + 2 * Math.PI - theta1;
	const antiClockwiseDistance = theta1 > theta2 ? theta1 - theta2 : theta1 + 2 * Math.PI - theta2;

	let resultDirection: number;

	if(clockwiseDistance < antiClockwiseDistance) {
		resultDirection = clockwiseDistance * getRotationThreshold(clockwiseDistance);
	}
	else {
		resultDirection = Constants.TWO_PI - antiClockwiseDistance * getRotationThreshold(antiClockwiseDistance);
	}

	return resultDirection;
}

export function getAbsoluteDirection(currentDirection: number,
			 relativeDirection: number): number {
	let resultDirection: number;

	const theta1: number = currentDirection;

	const theta2: number = relativeDirection;
	let theta = theta1 + theta2;

	while (theta > Constants.TWO_PI) {
		theta -= Constants.TWO_PI;
	}

	resultDirection = theta;

	return resultDirection;
}

export function getNextPoint(currentPoint: Point, direction: number): Point {
	let x: number, y:number;
	if(direction <= Constants.PI) {
		if(direction <= Constants.HALF_PI) {
			const angle = direction;
			x = currentPoint.x + Math.cos(angle) * Constants.SCALE;
			y = currentPoint.y + Math.sin(angle) * Constants.SCALE;
		}
		else {
			const angle = Constants.PI -  direction;
			x = currentPoint.x - Math.cos(angle) * Constants.SCALE;
			y = currentPoint.y + Math.sin(angle) * Constants.SCALE;
		}
	}
	else {
		const anti = Constants.TWO_PI - direction;
		if(anti <= Constants.HALF_PI) {
			const angle = anti;
			x = currentPoint.x + Math.cos(angle) * Constants.SCALE;
			y = currentPoint.y - Math.sin(angle) * Constants.SCALE;
		}
		else {
			const angle = Constants.PI -  anti;
			x = currentPoint.x - Math.cos(angle) * Constants.SCALE;
			y = currentPoint.y - Math.sin(angle) * Constants.SCALE;
		}
	}
	return new Point(x, y);
}

function getRotationThreshold(angle: number): number {
	return 1 - angle / Constants.PI;
}
