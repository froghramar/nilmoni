export class Constants {
	static PI =  Math.PI;
	static HALF_PI = Math.PI / 2;
	static TWO_PI = Math.PI * 2;
	static SCALE = 10;
	static SNAKE_WIDTH = 10;
}
